      
       
<br/>       
        
手机屏幕文字广告屏、干净LED牌
====
<img src="app/src/main/res/drawable/chatty.png" width="128"/><br/>
       
本例设备：     
小米note3       
MIUI 12.0.1      
Android 9     
<br/>


### 无任何权限申请
* 不需要imei、手机号、手机id等等，
* 不需要手机存储，
* 不需要录音、录像拍照等，
* 不需要联网，
* 唯一的一个锁屏时显示权限，没有涉及任何敏感操作。
<br/>
          
### 相关技术词
* ColorPicker。
* ButterKnife。
* 横竖屏。
* 跑马灯。
* 锁屏显示。
* 后台唤起。
* ScrollView和EditText嵌套时的滑动冲突。
<br/>

### 第三方代码保留有作者相关信息
* jianshu.com/users/81763e085b06/timeline
* gitee.com/necojack/MarqueeView
<br/>

🚀 测试apk：<br/>
<a href="https://gitee.com/vigiles/chattyboard/raw/master/apk/release/app-release-0.1.apk" target="_blank">附件0.1</a>
<br/>

设置横屏：<br/>
<img src="img/chattyboard_01.png" width="256"/>

设置竖屏：<br/>
<img src="img/chattyboard_02.png" width="256"/>

全屏显示：<br/>
<img src="img/chattyboard_03.png" width="256"/>
 <br/>
