package com.cuiweiyou.chattyboard;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.os.Bundle;
import android.os.PowerManager;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import com.cuiweiyou.chattyboard.serv.ScreenBroadcastReceiver;
import com.cuiweiyou.chattyboard.tool.ForeGroundTool;
import com.cuiweiyou.chattyboard.view.MarqueeView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

// https://gitee.com/necojack/MarqueeView
public class MarqueeActivity extends Activity {

    @BindView(R.id.landspaceMarquee)
    MarqueeView landspaceMarquee;

    @BindView(R.id.portraitMarquee1)
    MarqueeView portraitMarquee1;

    @BindView(R.id.portraitMarquee2)
    MarqueeView portraitMarquee2;

    @BindView(R.id.portraitMask)
    View portraitMask;

    @BindView(R.id.screenOnView)
    TextView screenOnView;

    private String backgroundColor = "#3F51B5";
    private String textColor = "#FEDFFE";
    private boolean isLandspace = true;
    private int textSize = 200;
    private int anmiSpeed = 3;
    private List<String> stringList = new ArrayList<>();
    private ScreenBroadcastReceiver receiver;
    private int screenLockedFlag = 2; // 0 黑屏了，1 亮屏了，2 解锁了
    private PowerManager.WakeLock wakeLock;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow()//
                .addFlags(//
                          WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED // 锁屏显示
                                  | WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD // 可以解锁
                                  | WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON   // 保持长亮
                                  | WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON); // 点亮屏幕

        setContentView(R.layout.activity_marquee);
        ButterKnife.bind(this);

        PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
        wakeLock = pm.newWakeLock( //
                                   PowerManager.ACQUIRE_CAUSES_WAKEUP  //
                                           | WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON  //
                                           | PowerManager.ON_AFTER_RELEASE,  //
                                   getClass().getName());

        receiver = new ScreenBroadcastReceiver(onScreenLockedListener);
        IntentFilter ifr = new IntentFilter();
        ifr.addAction(Intent.ACTION_SCREEN_ON);
        ifr.addAction(Intent.ACTION_SCREEN_OFF);
        ifr.addAction(Intent.ACTION_USER_PRESENT);
        registerReceiver(receiver, ifr);

        Bundle extras = getIntent().getExtras();
        backgroundColor = extras.getString("backgroundColor");
        textColor = extras.getString("textColor");
        isLandspace = extras.getBoolean("isLandspace");
        textSize = extras.getInt("textSize");
        anmiSpeed = extras.getInt("anmiSpeed");
        stringList = extras.getStringArrayList("stringList");

        if (isLandspace) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
            landspaceMarquee.setVisibility(View.VISIBLE);
            portraitMask.setVisibility(View.GONE);
        } else {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            landspaceMarquee.setVisibility(View.GONE);
            portraitMask.setVisibility(View.VISIBLE);
        }

        if (isLandspace) {
            landspaceMarquee.setText(stringList);
            landspaceMarquee.setTextSize(textSize * 2);
            landspaceMarquee.setTextSpeed(anmiSpeed);
            landspaceMarquee.setBackgroundColor(Color.parseColor(backgroundColor));
            landspaceMarquee.setTextColor(textColor);
        } else {
            int size = stringList.size();
            String str = stringList.get(0);
            portraitMarquee1.setText(str);
            if (size < 2) {
                portraitMarquee2.setText(str);
            } else {
                List<String> subList = stringList.subList(1, size);
                portraitMarquee2.setText(subList);
            }

            portraitMarquee1.setTextSize(textSize * 2);
            portraitMarquee1.setTextSpeed(anmiSpeed);
            portraitMarquee1.setBackgroundColor(Color.parseColor(backgroundColor));
            portraitMarquee1.setTextColor(textColor);

            portraitMarquee2.setTextSize(textSize * 2);
            portraitMarquee2.setTextSpeed(anmiSpeed);
            portraitMarquee2.setBackgroundColor(Color.parseColor(backgroundColor));
            portraitMarquee2.setTextColor(textColor);
        }

        screenOnView.setKeepScreenOn(true);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(receiver);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK || keyCode == KeyEvent.KEYCODE_HOME) {
            // 0 黑屏了，1 亮屏了，2 解锁了
            if (2 != screenLockedFlag) {
                return true;
            }
        }
        return super.onKeyDown(keyCode, event);
    }

    private ScreenBroadcastReceiver.OnScreenLockedListener onScreenLockedListener = new ScreenBroadcastReceiver.OnScreenLockedListener() {
        @Override
        public void onScreenLocked(int flag) {
            screenLockedFlag = flag;
            if (0 == screenLockedFlag) {// 黑屏了，点亮
                wakeLock.acquire(15000);
                if (wakeLock != null && wakeLock.isHeld()) {
                    wakeLock.release();
                }
            } else if (2 == screenLockedFlag) { // 解锁了会回到桌面，启动
                ForeGroundTool.foregroundActivity(MarqueeActivity.this, MarqueeActivity.class);
            }
        }
    };
}
