package com.cuiweiyou.chattyboard;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.TextView;

import com.cuiweiyou.chattyboard.app.ExitAppActivity;
import com.cuiweiyou.chattyboard.colorpicker.ColorPicker;
import com.cuiweiyou.chattyboard.colorpicker.OnColorPickedListener;
import com.cuiweiyou.chattyboard.view.MarqueeView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnCheckedChanged;
import butterknife.OnClick;
import butterknife.OnTextChanged;

public class MainActivity extends ExitAppActivity {

    @BindView(R.id.sizeSeek)
    SeekBar sizeSeek;

    @BindView(R.id.speedSeek)
    SeekBar speedSeek;

    @BindView(R.id.landspaceMarquee)
    MarqueeView landspaceMarquee;

    @BindView(R.id.portraitMarquee1)
    MarqueeView portraitMarquee1;

    @BindView(R.id.portraitMarquee2)
    MarqueeView portraitMarquee2;

    @BindView(R.id.portraitMask)
    View portraitMask;

    @BindView(R.id.labelTip)
    TextView labelTip;

    @BindView(R.id.editText)
    EditText editText;

    private String backgroundColor = "#3F51B5";
    private String textColor = "#FEDFFE";
    private boolean isLandspace = true;
    private int textSize = 200;
    private int anmiSpeed = 3;
    private ArrayList<String> stringList = new ArrayList<String>() {
        {
            add("请设置文字");
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        sizeSeek.setOnSeekBarChangeListener(seekChangeListener);
        speedSeek.setOnSeekBarChangeListener(seekChangeListener);
        sizeSeek.setProgress(textSize);
        speedSeek.setProgress(anmiSpeed);
        for (String str : stringList) {
            editText.append(str);
        }

        runMarquee();
    }

    @OnClick(R.id.colorpickViewBkg)
    public void onBkgColorButtonClick() {
        ColorPicker picker = new ColorPicker(this);
        picker.setInitColor(Color.parseColor(backgroundColor));
        picker.setOnColorPickListener(new OnColorPickedListener() {
            @Override
            public void onColorPicked(String hexRGBColor) {
                backgroundColor = hexRGBColor;
                landspaceMarquee.setBackgroundColor(Color.parseColor(hexRGBColor));
                portraitMarquee1.setBackgroundColor(Color.parseColor(hexRGBColor));
                portraitMarquee2.setBackgroundColor(Color.parseColor(hexRGBColor));
            }
        });
        picker.show();
    }

    @OnClick(R.id.colorpickViewTxt)
    public void onTxtColorButtonClick() {
        ColorPicker picker = new ColorPicker(this);
        picker.setInitColor(Color.parseColor(textColor));
        picker.setOnColorPickListener(new OnColorPickedListener() {
            @Override
            public void onColorPicked(String hexRGBColor) {
                textColor = hexRGBColor;
                landspaceMarquee.setTextColor(hexRGBColor);
                portraitMarquee1.setTextColor(hexRGBColor);
                portraitMarquee2.setTextColor(hexRGBColor);
            }
        });
        picker.show();
    }

    @OnCheckedChanged({R.id.landscapeRadio, R.id.portraitRadio})
    public void onDirectionRadioCheck(CompoundButton view, boolean ischanged) {
        switch (view.getId()) {
            case R.id.landscapeRadio:
                if (ischanged) {
                    isLandspace = true;
                    portraitMask.setVisibility(View.GONE);
                    landspaceMarquee.setVisibility(View.VISIBLE);
                    labelTip.setText("全部文字一行显示");
                }
                break;
            case R.id.portraitRadio:
                if (ischanged) {
                    isLandspace = false;
                    landspaceMarquee.setVisibility(View.GONE);
                    portraitMask.setVisibility(View.VISIBLE);
                    labelTip.setText("显示2行文字。第一行单独显示，其余行合并显示");
                }
                break;
            default:
                break;
        }

        runMarquee();
    }

    @OnTextChanged(value = R.id.editText, callback = OnTextChanged.Callback.AFTER_TEXT_CHANGED)
    public void onTextChanged(Editable s) {
        String string = s.toString().trim();
        if (!TextUtils.isEmpty(string)) {
            String[] split = string.split("\n");
            if (split.length > 0) {
                stringList.clear();
                for (String str : split) {
                    stringList.add(str);
                }

                runMarquee();
            }
        }
    }

    @OnClick(R.id.okBtn)
    public void onButtonClick() {
        Log.e("ard", "确定");
        Intent i = new Intent(this, MarqueeActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString("backgroundColor", backgroundColor);
        bundle.putString("textColor", textColor);
        bundle.putBoolean("isLandspace", isLandspace);
        bundle.putInt("textSize", textSize);
        bundle.putInt("anmiSpeed", anmiSpeed);
        bundle.putStringArrayList("stringList", stringList);
        i.putExtras(bundle);
        startActivity(i);
    }

    private SeekBar.OnSeekBarChangeListener seekChangeListener = new SeekBar.OnSeekBarChangeListener() {
        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {
        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {
            int id = seekBar.getId();
            switch (id) {
                case R.id.sizeSeek: {
                    int progress = sizeSeek.getProgress();
                    textSize = progress;
                    break;
                }
                case R.id.speedSeek: {
                    int progress = speedSeek.getProgress();
                    anmiSpeed = progress;
                    break;
                }
            }

            runMarquee();
        }
    };

    private void runMarquee() {
        if (isLandspace) {
            landspaceMarquee.setText(stringList);
            landspaceMarquee.setTextSize(textSize);
            landspaceMarquee.setTextSpeed(anmiSpeed);
            landspaceMarquee.setBackgroundColor(Color.parseColor(backgroundColor));
            landspaceMarquee.setTextColor(textColor);
        } else {
            int size = stringList.size();
            String str = stringList.get(0);
            portraitMarquee1.setText(str);
            if (size < 2) {
                portraitMarquee2.setText(str);
            } else {
                List<String> subList = stringList.subList(1, size);
                portraitMarquee2.setText(subList);
            }

            portraitMarquee1.setTextSize(textSize / 2);
            portraitMarquee1.setTextSpeed(anmiSpeed / 2f);
            portraitMarquee1.setBackgroundColor(Color.parseColor(backgroundColor));
            portraitMarquee1.setTextColor(textColor);

            portraitMarquee2.setTextSize(textSize / 2);
            portraitMarquee2.setTextSpeed(anmiSpeed / 2f);
            portraitMarquee2.setBackgroundColor(Color.parseColor(backgroundColor));
            portraitMarquee2.setTextColor(textColor);
        }
    }

    @Override
    public void whenAppExit() {

    }
}
