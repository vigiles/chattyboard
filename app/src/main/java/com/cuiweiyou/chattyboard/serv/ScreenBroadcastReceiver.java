package com.cuiweiyou.chattyboard.serv;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class ScreenBroadcastReceiver extends BroadcastReceiver {
    OnScreenLockedListener onScreenLockedListener;

    public ScreenBroadcastReceiver(OnScreenLockedListener listener) {
        onScreenLockedListener = listener;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        int flag = 0;
        if (action.equals(Intent.ACTION_SCREEN_OFF)) {
            flag = 0;
            Log.e("ard", "黑屏了"); // 去点亮
        } else if (action.equals(Intent.ACTION_SCREEN_ON)) {
            flag = 1;
            Log.e("ard", "亮屏了");
        } else if (action.equals(Intent.ACTION_USER_PRESENT)) {
            flag = 2;
            Log.e("ard", "解锁了");
        }

        if (null != onScreenLockedListener) {
            onScreenLockedListener.onScreenLocked(flag);
        }
    }

    public interface OnScreenLockedListener {
        /**
         * 锁屏回调
         *
         * @param flag 0 黑屏了，1 亮屏了，2 解锁了
         */
        void onScreenLocked(int flag);
    }
}
